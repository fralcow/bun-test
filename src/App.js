import "./App.css";
import paramjit from "./paramjit.png";
import styled from "styled-components";
import { useState } from "react";

const SpinningImage = styled.img`
  height: 40vmin;
  animation: App-logo-spin infinite ${(props) => props.$speed}s linear;
`;

function App() {
  const [speed, setSpeed] = useState(10);

  return (
    <div className="App">
      <header className="App-header">
        {speed > 0.01 ? (
          <SpinningImage
            $speed={speed}
            onClick={() => setSpeed(speed / 2)}
            src={paramjit}
          />
        ) : (
          <p> YOU BROKE PARAMJIT </p>
        )}
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
